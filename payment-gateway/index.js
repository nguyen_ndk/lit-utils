const {isFunction} = require('lodash')
const yopayment = require('./providers/yopayment')
const epay = require('./providers/epay')
const PAYMENT_GATEWAY_PROVIDER = {
  STRIPE: 'stripe',
  YOPAYMENT: 'yopayment',
  EPAY: 'epay',
}
class PaymentGatewayFacade {
  static provider(provider) {
    switch (provider) {
      case PAYMENT_GATEWAY_PROVIDER.YOPAYMENT:
        return yopayment
      case PAYMENT_GATEWAY_PROVIDER.EPAY:
        return epay
      default:
        return epay
    }
  }

  static saveCard(number, exp, cvc, currency, context) {
    const provider = PaymentGatewayFacade.provider()
    if (!isFunction(provider.saveCard)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION saveCard'))
    }
    return PaymentGatewayFacade.provider().saveCard(number, exp, cvc, currency, context)
  }

  // TODO replace this to v2
  static calcCreateTokenData(amount, ctx) {
    const provider = PaymentGatewayFacade.provider()
    if (!isFunction(provider.calcCreateTokenData)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION calcCreateTokenData'))
    }
    return PaymentGatewayFacade.provider().calcCreateTokenData(amount, ctx)
  }

  static getPaymentFormData(amount, ctx) {
    const provider = PaymentGatewayFacade.provider()
    if (!isFunction(provider.getPaymentFormData)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION getPaymentFormData'))
    }
    return PaymentGatewayFacade.provider().getPaymentFormData(amount, ctx)
  }
  static getMultiPurchasePaymentFormData(amount, ctx) {
    const provider = PaymentGatewayFacade.provider()
    if (!isFunction(provider.getMultiPurchasePaymentFormData)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION getMultiPurchasePaymentFormData'))
    }
    return PaymentGatewayFacade.provider().getMultiPurchasePaymentFormData(amount, ctx)
  }

  static getPMFormData(amount, ctx) {
    const provider = PaymentGatewayFacade.provider()
    if (!isFunction(provider.getPMFormData)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION getPMFormData'))
    }
    return PaymentGatewayFacade.provider().getPMFormData(amount, ctx)
  }

  static getNewPurchaseFormData(amount, ctx) {
    const provider = PaymentGatewayFacade.provider()
    if (!isFunction(provider.getNewPurchaseFormData)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION getNewPurchaseFormData'))
    }
    return PaymentGatewayFacade.provider().getNewPurchaseFormData(amount, ctx)
  }

  static charge(token, amount, currency, description, context) {
    const provider = PaymentGatewayFacade.provider()
    if (!isFunction(provider.charge)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION charge'))
    }
    return PaymentGatewayFacade.provider().charge(token, amount, currency, description, context)
  }

  static extractError(e) {
    return PaymentGatewayFacade.provider().extractError(e)
  }

  static tokenInit(userData, tranId) {
    const provider = PaymentGatewayFacade.provider()
    if (!isFunction(provider.tokenInit)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION tokenInit'))
    }
    return PaymentGatewayFacade.provider().tokenInit(userData, tranId)
  }

  static extractPayloadData(payload) {
    const provider = PaymentGatewayFacade.provider()
    if (!isFunction(provider.extractPayloadData)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION extractPayloadData'))
    }
    return PaymentGatewayFacade.provider().extractPayloadData(payload)
  }

  static tokenUnEnroll(pm, u, context) {
    const provider = PaymentGatewayFacade.provider()
    if (!isFunction(provider.tokenUnEnroll)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION tokenUnEnroll'))
    }
    return PaymentGatewayFacade.provider().tokenUnEnroll(pm, u, context)
  }

  static refund(amount, currency, context) {
    const provider = PaymentGatewayFacade.provider()
    if (!isFunction(provider.refund)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION refund'))
    }
    return PaymentGatewayFacade.provider().refund(amount, currency, context)
  }

  static inquiry(tranId) {
    const provider = PaymentGatewayFacade.provider()
    if (!isFunction(provider.inquiry)) {
      return Promise.reject(new Error('NON_SUPPORT_FUNCTION inquiry'))
    }
    return PaymentGatewayFacade.inquiry(tranId)
  }

  static getGatewayName(provider) {
    return PaymentGatewayFacade.provider(provider).getGatewayName()
  }

  static calcFee(amount, payType) {
    return PaymentGatewayFacade.provider().calcFee(amount, payType)
  }
}

module.exports = {
  PaymentGatewayFacade,
  PAYMENT_GATEWAY_PROVIDER
}
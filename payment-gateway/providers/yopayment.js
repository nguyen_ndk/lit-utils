const axios = require('axios');
const httpError = require('http-errors');
const _ = require('lodash');
const cryptojs = require('crypto-js');
const crypto = require('crypto');
const Ajv = require('ajv');
const ajv = new Ajv();
const tokenWebhookSchema = require('lit-schemas/yopayment/token-webhook');
const { Log } = require('../../log');
const { YOPAYMENT, FRONT_END_BASE_URL, errorTags } = require('lit-constants');
const request = axios.create({
  baseURL: YOPAYMENT.BASE_URL,
  headers: {
    Authorization: YOPAYMENT.PASSCODE,
    'Content-type': 'application/json',
    'X-Forwarded-For': YOPAYMENT.LIT_IP,
  },
});
/* eslint-disable camelcase */
class Yopayment {
  encrypt3des(text, key = YOPAYMENT.TRIPLE_DES_KEY) {
    key = cryptojs.enc.Utf8.parse(key);
    const encoded = cryptojs.enc.Utf8.parse(text);
    const ciphertext = cryptojs.TripleDES.encrypt(encoded, key, {
      mode: cryptojs.mode.ECB,
      padding: cryptojs.pad.Pkcs7,
    });
    return ciphertext.toString();
  }

  decrypt3des(data, key = YOPAYMENT.TRIPLE_DES_KEY) {
    key = cryptojs.enc.Utf8.parse(key);
    const ciphertext = cryptojs.TripleDES.decrypt(data, key, { mode: cryptojs.mode.ECB, padding: cryptojs.pad.Pkcs7 });
    return ciphertext.toString(cryptojs.enc.Utf8);
  }

  hash(text, secret) {
    return _.toUpper(crypto.createHmac('sha1', secret).update(text).digest('hex'));
  }

  getGatewayName() {
    return 'yopayment';
  }

  extractPayloadData(payload) {
    const validate = ajv.compile(tokenWebhookSchema);
    const valid = validate(payload);
    if (!valid) {
      const error = {
        message: _.map(validate.errors, (e) => _.pick(e, ['message', 'data', 'dataPath', 'propertyName'])),
        errorCode: YOPAYMENT.ERROR_CODE.INVALID_PARAM,
        TransactionID: payload.TransactionID,
      };
      throw httpError(400, JSON.stringify(error));
    }

    const checksumText = payload.TransactionID + payload.data + YOPAYMENT.MERCHANT_CODE + YOPAYMENT.PASSCODE;
    const checksum = this.hash(checksumText, YOPAYMENT.CHECKSUM_KEY);
    if (checksum !== payload.checksum) {
      const error = {
        message: 'Invalid checksum',
        errorCode: YOPAYMENT.ERROR_CODE.INVALID_CHECKSUM,
        TransactionID: payload.TransactionID,
      };
      throw httpError(400, JSON.stringify(error));
    }
    const data = JSON.parse(this.decrypt3des(payload.data));
    return {
      userId: _.get(data, 'customerID'),
      maskedCard: _.get(data, 'card_last_number'),
      token: _.get(data, 'token'),
      cardBrand: _.get(data, 'card_brand'),
      transactionId: _.get(payload, 'TransactionID'),
      status: _.get(data, 'status'),
      message: _.get(data, 'message'),
      gateway: this.getGatewayName(),
    };
  }

  async tokenInit(userData, tranId) {
    const logTag = `YOPAYMENT_TOKEN_INIT user: ${userData.id} transaction: ${tranId}`;
    const payload = {
      cancelURL: `${FRONT_END_BASE_URL}/yo/payment-method/result/cancel`,
      errorURL: `${FRONT_END_BASE_URL}/yo/payment-method/result/error`,
      resultURL: `${FRONT_END_BASE_URL}/yo/payment-method/result/success`,
      clientIP: '192.168.0.15',
      userAgent: 'Server',
      payment_service: 'CREATE TOKEN',
      totalAmount: 20000,
      customer_id: userData.id,
      TransactionID: tranId,
    };

    const dataObj = {
      custName: `${userData.id}`,
      custAddress: '',
      custGender: '',
      custEmail: `${userData.id}`,
      custPhone: `${userData.id}`,
      purse_payment: '1',
      customer_id: payload.customer_id,
      description: 'LITPAY create token for user: ' + userData.id,
      payment_type: 3,
    };
    payload.data = this.encrypt3des(JSON.stringify(dataObj), YOPAYMENT.TRIPLE_DES_KEY);
    const checksumText =
      payload.TransactionID +
      payload.data +
      payload.resultURL +
      payload.clientIP +
      payload.userAgent +
      YOPAYMENT.MERCHANT_CODE +
      YOPAYMENT.PASSCODE;
    payload.checksum = this.hash(checksumText, YOPAYMENT.CHECKSUM_KEY);
    Log.info(logTag, { payload, dataObj });
    const { data: res } = await request.post('/collection/create-order-token', payload).catch((e) => {
      Log.error(logTag, e);
      throw httpError(500, e);
    });
    Log.info(logTag, { res });
    const url = _.get(res, 'object.redirectURL');
    if (!url) throw httpError(500, errorTags.PAYMENT_GATEWAY_ERROR);
    return { url, data: payload.data, checksum: payload.checksum };
  }

  async tokenUnEnroll(pm, u, context) {
    const logTag = `YOPAYMENT_UN_ENROLL_TOKEN user: ${u.id} transaction: ${context.tranId}`;
    const payload = {
      TransactionID: context.tranId,
      clientIP: '115.79.28.221',
      userAgent: 'server',
    };

    const dataObj = {
      customer_id: `${u.id}`,
      custName: `${u.id}`,
      custGender: '',
      custEmail: `${u.id}`,
      custPhone: `${u.id}`,
      purse_payment: '1',
      description: `UN-ENROLL TOKEN ${pm.bankToken} user ${u.id}`,
      token_type: 3,
      token: pm.bankToken,
    };
    payload.data = this.encrypt3des(JSON.stringify(dataObj), YOPAYMENT.TRIPLE_DES_KEY);
    const checksumText =
      payload.TransactionID +
      payload.data +
      payload.clientIP +
      payload.userAgent +
      YOPAYMENT.MERCHANT_CODE +
      YOPAYMENT.PASSCODE;
    payload.checksum = this.hash(checksumText, YOPAYMENT.CHECKSUM_KEY);

    Log.info(logTag, { payload });
    const { data: response } = await request.post('/collection/remove-token', payload);
    if (response.errorCode !== '0') {
      Log.error(logTag, { error: response, payload, dataObj });
      throw httpError(500, errorTags.PAYMENT_GATEWAY_ERROR);
    }
  }

  async charge(token, amount, currency, description, context) {
    // TODO add installment or purchase info in here
    const logTag = `YOPAYMENT_CHANGE user: ${context.userId} transaction: ${context.tranId}`;
    const payload = {
      title: _.truncate(`user: ${context.userId}`, { length: YOPAYMENT.LIMITATION.TITLE.LENGTH }),
      payment_service: 'BATCH',
      TransactionID: context.batchTranId,
    };
    const customer = {
      custName: `${context.userId}`,
      custPhone: `${context.userId}`,
      customer_id: context.userId,
      token,
      purse_payment: '1',
      description: _.truncate(description, { length: YOPAYMENT.LIMITATION.DESCRIPTION.LENGTH }),
      mTransactionID: context.tranId,
      amount,
      merchant_fee: 0,
      payment_type: 3,
    };
    payload.data = this.encrypt3des(JSON.stringify(customer), YOPAYMENT.TRIPLE_DES_KEY);
    const checksumText = payload.data + payload.title + YOPAYMENT.MERCHANT_CODE + YOPAYMENT.PASSCODE;
    payload.checksum = this.hash(checksumText, YOPAYMENT.CHECKSUM_KEY);
    Log.info(logTag, { payload });
    const { data: res } = await request.post('/collection/create-transaction', payload);
    if (res.errorCode !== '0') {
      Log.error(logTag, { payload, res, customer });
      throw httpError(500, errorTags.PAYMENT_GATEWAY_FAILED_TO_CHARGE);
    }
    return {};
  }

  async refund(amount, currency, description, context) {
    // TODO add installment or purchase info in here
    const logTag = `YOPAYMENT_REFUND user: ${context.userId} transaction: ${context.tranId}`;
    const payload = {
      title: _.truncate(`user: ${context.userId}`, { length: YOPAYMENT.LIMITATION.TITLE.LENGTH }),
    };
    const data = {
      mTransactionID: `${context.tranId}`,
      totalAmount: amount,
      customer_id: `${context.userId}`,
    };
    payload.data = this.encrypt3des(JSON.stringify(data), YOPAYMENT.TRIPLE_DES_KEY);
    const checksumText = payload.data + payload.title + YOPAYMENT.MERCHANT_CODE + YOPAYMENT.PASSCODE;
    payload.checksum = this.hash(checksumText, YOPAYMENT.CHECKSUM_KEY);
    Log.info(logTag, { payload });
    const { data: res } = await request.post('/collection/refund-transaction', payload);
    if (res.errorCode !== '0') {
      Log.error(logTag, { payload, res, data });
      throw httpError(500, errorTags.PAYMENT_GATEWAY_FAILED_TO_REFUND);
    }
  }

  extractError(e) {
    return _.pick(e, ['errorCode', 'message', 'uiMessage', 'tranError']);
  }
}

module.exports = new Yopayment();

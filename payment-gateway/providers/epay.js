const _ = require('lodash')
const crypto = require('crypto')
const axios = require('axios')
const moment = require('moment')
const qs = require('querystring')
const httpError = require('http-errors')
const helpers = require('../../helpers')
const {epay: epaySchema} = require('lit-schemas')
const {Log} = require('../../log')
const {EPAY, DEFAULT_CURRENCY, FRONT_END_BASE_URL, BASE_URL, errorTags, PAYMENT_METHOD, PAYMENT_GATEWAY_REQUEST,
  MAP_CARD_TYPE_TO_PAY_OPTION} = require('lit-constants')

const request = axios.create({
  baseURL: EPAY.DOMAIN,
})

function tripledesEncrypt(param) {
  const key = new Buffer(param.key)
  const iv = new Buffer(param.iv ? param.iv : 0)
  const plaintext = param.plaintext
  const alg = param.alg

  const cipher = crypto.createCipheriv(alg, key, iv)
  let ciph = cipher.update(plaintext, 'utf8', 'hex')
  ciph += cipher.final('hex')
  return ciph
}

function tripledesDecrypt(param) {
  const key = new Buffer(param.key)
  const iv = new Buffer(param.iv ? param.iv : 0)
  const alg = param.alg

  //decrypt
  const decipher = crypto.createDecipheriv(alg, key, iv)
  let txt = decipher.update(param.ciph, 'hex', 'utf8')
  txt += decipher.final('utf8')
  return txt
}
class Epay {
  encrypt3des (mytext) {
    return tripledesEncrypt(
      {
        alg: 'des-ede3', //3des-ecb
        autoPad: false,
        key: EPAY.KEY3DES_ENCRYPT,
        plaintext: mytext,
        iv: null
      })
  }
  decrypt3des (encrypted) {
    return tripledesDecrypt({
      alg: 'des-ede3', //3des-ecb
      autoPad: false,
      key: EPAY.KEY3DES_DECRYPT,
      ciph: encrypted,
      iv: null
    })
  }

  hash(text) {
    return crypto
      .createHash('sha256')
      .update(text)
      .digest('hex')
  }

  getGatewayName() {
    return 'epay'
  }

  extractPayloadData(payload) {
    let checksumText
    if (payload.resultCd === '00_000' && !payload.payToken) {
      checksumText = payload.resultCd
        + payload.timeStamp
        + payload.merTrxId
        + payload.trxId
        + EPAY.MERCHANT_ID
        + payload.amount
        + EPAY.ENCODE_KEY
    } else if (payload.payToken) {
      checksumText = payload.resultCd
        + payload.timeStamp
        + payload.merTrxId
        + payload.trxId
        + EPAY.MERCHANT_ID
        + payload.amount
        + payload.payToken
        + EPAY.ENCODE_KEY
    } else {
      throw httpError(400, errorTags.EPAY_MISSING_PAY_TOKEN)
    }
    const merchantToken = this.hash(checksumText)
    if (merchantToken !== payload.merchantToken) {
      throw httpError(400, errorTags.EPAY_INVALID_MERCHANT_TOKEN)
    }
    return {
      userId: _.get(payload, 'userId'),
      maskedCard: _.get(payload, 'cardNo'),
      token: _.get(payload, 'payToken'),
      cardBrand: _.get(payload, 'issueBankName'),
      transactionId: _.get(payload, 'merTrxId'),
      gatewayTranId: _.get(payload, 'trxId'),
      status: _.get(payload, 'status'),
      type: EPAY.MAP_CARD_TYPE[_.get(payload, 'payType')],
      message: _.get(payload, 'resultMsg'),
      gateway: this.getGatewayName()
    }
  }

  _getPayOption(type) {
    switch (type) {
      case PAYMENT_GATEWAY_REQUEST.TYPE.CHARGE_QR_3D_SECURE:
      case PAYMENT_GATEWAY_REQUEST.TYPE.CHARGE_AIO_3D_SECURE:
        return EPAY.PAY_OPTIONS.PURCHASE_OTP

      default:
        return EPAY.PAY_OPTIONS.PAY_AND_CREATE_TOKEN
    }
  }

  _getNotiUrl(type) {
    return  `${BASE_URL}${EPAY.NOTIFY_URL_BY_TYPE[type]}`
  }

  _getPayToken(ctx) {
    if (!ctx.bankToken) return ''
    const clearToken = this.decrypt3des(ctx.bankToken)
    return this.encrypt3des(clearToken)
  }

  _getMerchantToken(amount, ctx, timeStamp, payToken) {
    let checksumText
    switch (ctx.type) {
      case PAYMENT_GATEWAY_REQUEST.TYPE.CHARGE_QR_3D_SECURE:
      case PAYMENT_GATEWAY_REQUEST.TYPE.CHARGE_AIO_3D_SECURE:
        if (!ctx.bankToken) throw httpError(400, errorTags.EPAY_MISSING_PAY_TOKEN)
        checksumText = timeStamp
          + ctx.tranId
          + EPAY.MERCHANT_ID
          + amount
          + payToken
          + EPAY.ENCODE_KEY
        break

      default:
        checksumText = timeStamp
          + ctx.tranId
          + EPAY.MERCHANT_ID
          + amount
          + EPAY.ENCODE_KEY
    }
    return this.hash(checksumText)
  }

  getPaymentMerchantToken(amount, ctx) {
    const checksumText = ctx.timeStamp
      + ctx.tranId
      + EPAY.MERCHANT_ID
      + amount
      + (ctx.payToken || '')
      + EPAY.ENCODE_KEY
    return this.hash(checksumText)
  }

  async getPaymentFormData(amount, ctx) {
    if (!ctx.bankToken) throw httpError(400, errorTags.EPAY_MISSING_PAY_TOKEN)
    ctx.notiUrl = `${BASE_URL}/weho/ep/payment`
    ctx.payType = EPAY.MAP_CARD_TYPE_TO_PAY_TYPE[ctx.paymentMethodType]
    ctx.payOption = EPAY.PAY_OPTIONS.PURCHASE_OTP
    return this.getFormData(amount, ctx)
  }

  async getNewPurchaseFormData(amount, ctx) {
    if (!ctx.bankToken) throw httpError(400, errorTags.EPAY_MISSING_PAY_TOKEN)
    ctx.notiUrl = `${BASE_URL}/weho/ep/new-purchase`
    ctx.payType = EPAY.MAP_CARD_TYPE_TO_PAY_TYPE[ctx.paymentMethodType]
    ctx.payOption = EPAY.PAY_OPTIONS.PURCHASE_OTP
    return this.getFormData(amount, ctx)
  }

  async getMultiPurchasePaymentFormData(amount, ctx) {
    if (!ctx.bankToken) throw httpError(400, errorTags.EPAY_MISSING_PAY_TOKEN)
    ctx.notiUrl = `${BASE_URL}/weho/ep/multi-purchase-payment`
    ctx.payType = EPAY.MAP_CARD_TYPE_TO_PAY_TYPE[ctx.paymentMethodType]
    ctx.payOption = EPAY.PAY_OPTIONS.PURCHASE_OTP
    return this.getFormData(amount, ctx)
  }

  async getPMFormData(amount, ctx) {
    ctx.notiUrl = `${BASE_URL}/weho/ep/add-pm`
    ctx.payType = EPAY.MAP_CARD_TYPE_TO_PAY_TYPE[ctx.paymentMethodType]
    ctx.payOption = MAP_CARD_TYPE_TO_PAY_OPTION[ctx.paymentMethodType]
    return this.getFormData(amount, ctx)
  }

  async getFormData(amount, ctx) {
    ctx.timeStamp = moment().format('YMMDDHms')
    ctx.payToken = this._getPayToken(ctx)
    const data = {
      merId: EPAY.MERCHANT_ID,
      currency: _.toUpper(DEFAULT_CURRENCY),
      amount: `${amount}`,
      invoiceNo: ctx.tranId,
      goodsNm: ctx.payOption,
      buyerEmail: '',
      payType: ctx.payType,
      callBackUrl: `${FRONT_END_BASE_URL}/notify-payment?purchaseId=${ctx.purchaseId}`,
      notiUrl: ctx.notiUrl,
      domain: EPAY.DOMAIN,
      vat: 0,
      fee: 0,
      description: `transaction: ${ctx.tranId}`,
      merchantToken: this.getPaymentMerchantToken(amount, ctx),
      userIP: ctx.ip,
      userLanguage: 'VN',
      timeStamp: ctx.timeStamp,
      merTrxId: ctx.tranId,
      userFee: '0',
      goodsAmount: `${amount}`,
      windowColor: '#06098a',
      windowType: EPAY.WINDOW_TYPE_MAP[ctx.windowType],
      payOption: ctx.payOption,
      userId: ctx.userId
    }
    if (ctx.payToken) data.payToken = ctx.payToken
    return helpers.truncateJson(data, epaySchema.payAndCreateToken)
  }

  async calcCreateTokenData(amount, ctx) {
    const timestamp = moment().format('YMMDDHms')
    const notiUrl = this._getNotiUrl(ctx.type)
    const payType = EPAY.MAP_CARD_TYPE_TO_PAY_TYPE[ctx.paymentMethodType]
    const payOption = this._getPayOption(ctx.type)
    const payToken = this._getPayToken(ctx)
    const data = {
      merId: EPAY.MERCHANT_ID,
      currency: _.toUpper(DEFAULT_CURRENCY),
      amount: `${amount}`,
      invoiceNo: ctx.tranId,
      goodsNm: payOption,
      buyerEmail: '',
      payType: payType,
      callBackUrl: `${FRONT_END_BASE_URL}/notify-payment?purchaseId=${ctx.purchaseId}`,
      notiUrl,
      domain: EPAY.DOMAIN,
      vat: 0,
      fee: 0,
      description: `transaction: ${ctx.tranId}`,
      merchantToken: this._getMerchantToken(amount, ctx, timestamp, payToken),
      userIP: ctx.ip,
      userLanguage: 'VN',
      timeStamp: timestamp,
      merTrxId: ctx.tranId,
      userFee: '0',
      goodsAmount: `${amount}`,
      windowColor: '#06098a',
      windowType: EPAY.WINDOW_TYPE_MAP[ctx.windowType],
      payOption: payOption,
      userId: ctx.userId
    }
    if (payToken) data.payToken = payToken
    return helpers.truncateJson(data, epaySchema.payAndCreateToken)
  }

  async charge(token, amount, currency, description, context) {
    const logTag = `EPAY_CHARGE user ${context.userId} transaction ${context.tranId}`
    const timeStamp = moment().format('YMMDDHms')
    const clearToken = this.decrypt3des(token)
    const payToken = this.encrypt3des(clearToken)
    const checksumText = timeStamp
      + context.tranId
      + EPAY.MERCHANT_ID
      + amount
      + payToken
      + EPAY.ENCODE_KEY
    const merchantToken = this.hash(checksumText)

    const raw = {
      merId: EPAY.MERCHANT_ID,
      merTrxId: context.tranId,
      amount: `${amount}`,
      goodsAmount: `${amount}`,
      userFee: '0',
      currency: _.toUpper(DEFAULT_CURRENCY),
      payType: EPAY.PAY_TYPE.IC,
      timeStamp,
      vat: '0',
      fee: '0',
      notax: '0',
      invoiceNo: context.tranId,
      goodsNm: 'PAY_WITH_TOKEN_API',
      notiUrl: `${BASE_URL}/weho/ep/ToAnPa`,
      merchantToken,
      payOption: EPAY.PAY_OPTIONS.PAY_WITH_TOKEN_API,
      payToken: payToken,
      userId: `${context.userId}`,
    }
    const payload = helpers.truncateJson(raw, epaySchema.charge)
    Log.info(logTag, {payload})
    const {data} = await request.post('/pg_was/payWithTokenAPI.do', payload)
    if (data.resultCd !== EPAY.RESPONSE_CODE.SUCCESS) {
      Log.error(logTag, {response: data})
      throw httpError(500, errorTags.PAYMENT_GATEWAY_FAILED_TO_CHARGE)
    }
    return {gatewayTranId: data.trxId}
  }

  async inquiry(tranId) {
    const logTag = `EPAY_INQUIRY transaction ${tranId}`
    const timeStamp = moment().format('YMMDDHms')
    const checksumText = timeStamp
      + tranId
      + EPAY.MERCHANT_ID
      + EPAY.ENCODE_KEY
    const merchantToken = this.hash(checksumText)

    const raw = {
      merId: EPAY.MERCHANT_ID,
      merTrxId: tranId,
      timeStamp,
      merchantToken,
    }
    const payload = helpers.truncateJson(raw, epaySchema.inquiry)
    const payloadStr = '&merId=' + payload.merId +
      '&merTrxId=' + payload.merTrxId +
      '&timeStamp=' + payload.timeStamp +
      '&merchantToken=' + payload.merchantToken
    Log.info(logTag, payloadStr)
    const {data} = await request.post('/pg_was/order/trxStatus.do', payloadStr)

    if (data.resultCd !== EPAY.RESPONSE_CODE.SUCCESS) {
      Log.error(logTag, {response: data})
      throw httpError(500, errorTags.PAYMENT_GATEWAY_FAILED_TO_INQUIRY)
    }
    const status = _.get(data, 'data.status')
    switch (status) {
      case EPAY.STATUS.NOT_FOUND:
        return {notExist: true}
      case EPAY.STATUS.PENDING:
        return {notExist: false, pending: true}
      default:
        return {notExist: false, pending: false, data: data.data}
    }
  }



  async refund(amount, currency, ctx) {
    const logTag = `EPAY_CHARGE user ${ctx.userId} transaction ${ctx.refundTranId}`
    const timeStamp = moment().format('YMMDDHms')
    const checksumText = timeStamp
      + ctx.refundTranId
      + ctx.gatewayTranId
      + EPAY.MERCHANT_ID
      + amount
      + EPAY.ENCODE_KEY
    const merchantToken = this.hash(checksumText)

    const raw = {
      trxId: ctx.gatewayTranId,
      merId: EPAY.MERCHANT_ID,
      merTrxId: ctx.refundTranId,
      amount: `${amount}`,
      payType: EPAY.MAP_CARD_TYPE_TO_PAY_TYPE[ctx.paymentMethodType],
      timeStamp,
      merchantToken,
      cancelPw: EPAY.CANCEL_PASSWORD
    }
    const payload = helpers.truncateJson(raw, epaySchema.refund)

    const payloadStr = 'trxId=' + payload.trxId +
      '&merId=' + payload.merId +
      '&merTrxId=' + payload.merTrxId +
      '&amount=' + payload.amount +
      '&payType=' + payload.payType +
      '&timeStamp=' + payload.timeStamp +
      '&merchantToken=' + payload.merchantToken +
      '&cancelMsg=cancel' +
      '&cancelPw=' +  qs.escape(payload.cancelPw)
    Log.info(logTag, {payloadStr})
    const {data} = await request.post('/pg_was/cancel/paymentCancel.do', payloadStr)
    if (data.resultCd !== EPAY.RESPONSE_CODE.SUCCESS) {
      Log.error(logTag, {response: data})
      throw httpError(500, errorTags.PAYMENT_GATEWAY_FAILED_TO_REFUND)
    }
    return {gatewayTranId: data.cancelTrxId}
  }

  calcFee(amount, payType) {
    switch (payType) {
      case PAYMENT_METHOD.TYPE.ATM:
        return amount * EPAY.ATM_PERCENTAGE / 100 + EPAY.ATM_AMOUNT
      case PAYMENT_METHOD.TYPE.CREDIT_CARD:
      default:
        return amount * EPAY.CREDIT_PERCENTAGE / 100 + EPAY.CREDIT_AMOUNT
    }
  }

  extractError(e) {
    return e
  }
}

module.exports = new Epay()
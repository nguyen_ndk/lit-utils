const { DBFacade, DB_PROVIDER } = require('./database/index');
const { EncryptFacade, ENCRYPT_PROVIDER } = require('./encrypt');
const { FileFacade, FILE_PROVIDER } = require('./file');
const { KycFacade, KYC_PROVIDER } = require('./kyc');
const { EmailFacade } = require('./mail');
const { PaymentGatewayFacade, PAYMENT_GATEWAY_PROVIDER } = require('./payment-gateway');
const { QueueFacade, QUEUE_PROVIDER } = require('./queue');
const { TokenFacade, TOKEN_PROVIDER } = require('./token');
const { Log, LOG_PROVIDER } = require('./log');
const { PushNotification, PUSH_NOTIFICATION_PROVIDER } = require('./push-notification');
const { Accounting, ACCOUNTING_PROVIDER } = require('./accounting');
const helpers = require('./helpers');
const OnlineStoreHelper = require('./online-store');
const SmsFacade = require('./sms');

module.exports = {
  facade: {
    DBFacade,
    EncryptFacade,
    FileFacade,
    KycFacade,
    EmailFacade,
    PaymentGatewayFacade,
    QueueFacade,
    TokenFacade,
    Log,
    PushNotification,
    Accounting,
    SmsFacade,
  },
  providers: {
    DB_PROVIDER,
    ENCRYPT_PROVIDER,
    FILE_PROVIDER,
    KYC_PROVIDER,
    PAYMENT_GATEWAY_PROVIDER,
    QUEUE_PROVIDER,
    TOKEN_PROVIDER,
    LOG_PROVIDER,
    PUSH_NOTIFICATION_PROVIDER,
    ACCOUNTING_PROVIDER,
  },
  helpers,
  OnlineStoreHelper,
};

const sequelize = require('./providers/sequelize')
const DB_PROVIDER = {
  SEQUELIZE: 'sequelize'
}

class DBFacade {
  static provider(provider) {
    switch (provider) {
      case DB_PROVIDER.SEQUELIZE:
        return sequelize
      default:
        return sequelize
    }
  }

  static connect() {
    return DBFacade.provider().connect()
  }
  static transaction() {
    return DBFacade.provider().transaction()
  }
  static commit() {
    return DBFacade.provider().commit()
  }
  static rollback() {
    return DBFacade.provider().rollback()
  }
  static getConnection() {
    return DBFacade.provider().getConnection()
  }
}

module.exports = {
  DBFacade,
  DB_PROVIDER
}
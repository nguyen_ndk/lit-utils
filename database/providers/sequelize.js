const {Sequelize} = require('sequelize')
const {MYSQL} = require('lit-constants')

const sequelize = new Sequelize(MYSQL.DATABASE, MYSQL.USER, MYSQL.PASSWORD, {
  host: MYSQL.HOST,
  dialect: 'mysql',
  port: MYSQL.PORT,
  timezone: '+07:00',
  pool: {
    max: 100,
    min: 0,
    acquire: 60000,
    idle: 10000,
  },
  logging: () => {}
})

const connect = () => sequelize.authenticate()

const transaction = () => sequelize.transaction()

const commit = () => sequelize.commit()

const rollback = () => sequelize.rollback()
const getConnection = () => sequelize

module.exports = {
  connect,
  transaction,
  commit,
  rollback,
  getConnection
}

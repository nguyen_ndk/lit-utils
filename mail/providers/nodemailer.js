const nodemailer = require('nodemailer')
const aws = require('aws-sdk')
const {DEVOPS_EMAILS, AWS} = require('lit-constants')
class NodeMailer {
  constructor() {
    this.transporter = null	
  }

  getAccount() {
    const ses = new aws.SES({
      accessKeyId: AWS.ACCESS_KEY,
      secretAccessKey: AWS.ACCESS_SECRET,
      region: AWS.REGION,
    })

    // create reusable transporter object using the default SMTP transport
    this.transporter = nodemailer.createTransport({
      SES: ses
    })
  }
  async send(from, to, subject, body) {
    if (!this.transporter) this.getAccount()
    await this.transporter.sendMail({
      from,
      to,
      subject,
      html: body,
      bcc: DEVOPS_EMAILS
    })
  }
}
module.exports = NodeMailer

const {
  XeroClient,
  Contacts,
  Invoices,
  Payments,
} = require('xero-node')
const {BASE_URL, CONFIGURATION, ADMIN_FE_BASE_URL, PURCHASE_ACCOUNTING, XERO} = require('lit-constants')
const httpError = require('http-errors')
const _ = require('lodash')
const scopes = 'offline_access openid profile email accounting.transactions accounting.transactions.read accounting.reports.read accounting.journals.read accounting.settings accounting.settings.read accounting.contacts accounting.contacts.read accounting.attachments accounting.attachments.read files files.read assets assets.read projects projects.read payroll.employees payroll.payruns payroll.payslip payroll.timesheets payroll.settings'
const xero = new XeroClient({
  clientId: XERO.CLIENT_ID,
  clientSecret: XERO.SECRET,
  redirectUris: [`${BASE_URL}/Ad/XeSaTo`],
  scopes: scopes.split(' '),
  state: 'imaParam=look-at-me-go',
  httpTimeout: 2000
})
const buildData = require('./xero/build-data')
const extractInvoiceId = require('./xero/extract-invoice-id')
const extractPaymentId = require('./xero/extract-payment-id')

const getLoginUrl = () => {
  return xero.buildConsentUrl()
}

const decode = async (url) => {
  await getLoginUrl() // this is a error from xero, leave this alone
  const tokenSet = await xero.apiCallback(url)
  return JSON.stringify(tokenSet)
}

const syncUsers = async (users, auth) => {
  return _syncContacts(users, auth)
}

const refreshToken = async (auth) => {
  const tokenSet = JSON.parse(auth)
  await getLoginUrl() // this is a error from xero, leave this alone
  await xero.setTokenSet(tokenSet)
  const newTokenSet = await xero.refreshToken()
  return JSON.stringify(newTokenSet)
}

const _syncContacts = async (contacts, auth) => {
  let newTokenSet
  const tokenSet = JSON.parse(auth)
  try {
    await getLoginUrl() // this is a error from xero, leave this alone
    await xero.setTokenSet(tokenSet)
    if (tokenSet.expires_at * 1000 < Date.now()) {
      newTokenSet = await xero.refreshToken()
    }
    const store = new Contacts()

    store.contacts = contacts
    const {body} = await xero.accountingApi.updateOrCreateContacts(XERO.TENANT_ID, store, false)
    const result = {data: body}
    if (newTokenSet) {
      result.newAuth = JSON.stringify(newTokenSet)
    }
    return result
  } catch (e) {
    throw _extractError(e)
  }
}

const syncMerchants = async (merchants, auth) => {
  return _syncContacts(merchants, auth)
}


const _extractError = (e) => {
  const res = _.get(e, 'response', {})
  const body = res.body || res
  console.log(e)
  return httpError(body.Status || body.statusCode || 500, body.Detail || body.Message || body.statusMessage)
}

const buildUserData = (user) => {
  return {
    contactNumber: user.id,
    contactStatus: 'ACTIVE',
    firstName: user.firstName,
    lastName: user.lastName,
    emailAddress: user.email,
    salesDefaultAccountCode: XERO.REV_USER_RECEIVABLE,
    isCustomer: true,
    isSupplier: false,
    website: `${ADMIN_FE_BASE_URL}/users/${user.id}`,
    name: `${user.lastName} ${user.firstName}`,
  }
}

const buildMerchantData = (merchant) => {
  const contacts = _.get(merchant, 'MerchantContacts', [])
  if (_.isEmpty(contacts)) return {}
  const result =  {
    name: merchant.name,
    firstName: contacts[0].name,
    lastName: '',
    emailAddress: contacts[0].email1,
    contactPersons: [],
    purchasesDefaultAccountCode: XERO.COST_MERCHANT,
    contactNumber: merchant.id,
    contactStatus: 'ACTIVE',
    isCustomer: false,
    isSupplier: true,
    website: `${ADMIN_FE_BASE_URL}/merchants/${merchant.id}`,
  }
  _.forEach(contacts, c => {
    result.contactPersons.push({
      firstName: c.name,
      lastName: '',
      emailAddress: c.email1,
      includeInEmails: 'true'
    })
  })

  return result
}



const _syncPurchaseInvoices = async (purchase) => {
  const invoices = buildData.buildPurchaseData(purchase)
  if (_.isEmpty(invoices)) {
    throw new Error('IN_VALID_PURCHASE')
  }
  const merchant = _.get(purchase, 'Merchant')
  let store = new Invoices()

  store.invoices = invoices
  const {body} = await xero.accountingApi.updateOrCreateInvoices(XERO.TENANT_ID, store, false)
  return extractInvoiceId(purchase, merchant, body)
}

const _syncPayments = async (purchaseInvoiceId, adminFeeInvoiceId, purchase, installments) => {
  const merchant = _.get(purchase, 'Merchant')
  let payments = []
  if (!_.isEmpty(installments)) {
    _.forEach(installments, i => {
      payments.push(buildData.buildInstallmentPayment(i, purchaseInvoiceId, purchase, merchant))
    })
  }
  const adminFeePayment = buildData.buildAdminFeePayment(purchase, adminFeeInvoiceId)
  const adminFeeReference = adminFeePayment.reference
  payments.push(adminFeePayment)
  const store = new Payments()
  store.payments = payments
  const {body} = await xero.accountingApi.createPayments(XERO.TENANT_ID, store, false)
  return extractPaymentId(body, adminFeeReference, purchase.id)
}

const syncPurchase = async (purchase, installments, auth) => {
  let newTokenSet
  const tokenSet = JSON.parse(auth)
  try {
    await getLoginUrl() // this is a error from xero, leave this alone
    await xero.setTokenSet(tokenSet)
    if (tokenSet.expires_at * 1000 < Date.now()) {
      newTokenSet = await xero.refreshToken()
    }
    const {invoiceIds, purchaseInvoiceId, adminFeeInvoiceId} = await _syncPurchaseInvoices(purchase)
    const paymentIds = await _syncPayments(purchaseInvoiceId, adminFeeInvoiceId, purchase, installments)
    const result = {
      financeIds: [...invoiceIds, ...paymentIds],
    }
    if (newTokenSet) {
      result.newAuth = JSON.stringify(newTokenSet)
    }
    return result
  } catch (e) {
    throw _extractError(e)
  }
}

const syncPaymentGatewayFee = async (desc, amount, user, auth) => {
  const tokenSet = JSON.parse(auth)
  await getLoginUrl() // this is a error from xero, leave this alone
  await xero.setTokenSet(tokenSet)
  if (tokenSet.expires_at * 1000 < Date.now()) {
    await xero.refreshToken()
  }
  try {
    const invoice = buildData.buildPaymentGatewayFeeInvoices(desc, amount, user.financeId)
    const invoiceStore = new Invoices()
    invoiceStore.invoices = [invoice]
    const {body} = await xero.accountingApi.updateOrCreateInvoices(XERO.TENANT_ID, invoiceStore, false)
    const invoiceId = body.invoices[0].invoiceID
    console.log('invoiceId', invoiceId)
    const payment = buildData.buildPaymentGatewayPayment(amount, invoiceId, user.id)
    const paymentStore = new Payments()
    paymentStore.payments = [payment]
    await xero.accountingApi.createPayments(XERO.TENANT_ID, paymentStore, false)
    return {
      financeId: invoiceId,
      amount: body.invoices[0].total,
      status: body.invoices[0].status,
      label: PURCHASE_ACCOUNTING.LABEL.PAYMENT_GATEWAY_PAYABLE_INVOICE_ID
    }
  } catch (e) {
    throw _extractError(e)
  }
}

const authKey = CONFIGURATION.FIELD_NAME.XERO_TOKEN_SET

module.exports = {
  getLoginUrl,
  decode,
  syncUsers,
  buildUserData,
  buildMerchantData,
  refreshToken,
  syncMerchants,
  syncPurchase,
  syncPaymentGatewayFee,
  authKey,
}
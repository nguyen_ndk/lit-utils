const _ = require('lodash')
const {PURCHASE_ACCOUNTING} = require('lit-constants')
module.exports = (body, adminFeeReference, purchaseId) => {
  const result = []
  _.forEach(body.payments, payment => {
    if (!_.isEmpty(payment.validationErrors)) {
      // TODO slack notification
      // Log.info(`XERO_EXTRACT_INSTALLMENT_FINANCE_ID REFERENCE ${payment.reference}`, payment.validationErrors)
      return
    }
    const data = {
      financeId: payment.paymentID,
      purchaseId,
      status: payment.status,
      amount: payment.amount
    }
    if (payment.reference === adminFeeReference) {
      data.label = PURCHASE_ACCOUNTING.LABEL.ADMIN_FEE_PAYMENT_ID
      return
    } else {
      data.installmentId = payment.reference
      data.label = PURCHASE_ACCOUNTING.LABEL.INSTALLMENT_PAYMENT_ID
    }
    result.push(data)
  })
  return result
}
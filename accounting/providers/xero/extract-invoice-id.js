const buildData = require('./build-data')
const _ = require('lodash')
const {PURCHASE_ACCOUNTING} = require('lit-constants')
module.exports = (purchase, merchant, body) => {
  const purchaseInvoiceNumber = buildData.genPurchaseInvoiceNumber(purchase.id)
  const adminFeeInvoiceNumber = buildData.genAdminFeeInvoiceNumber(purchase.id)
  const merchantPayInvoiceNumber = buildData.genMerchantPayInvoiceNumber(purchase.id, merchant.id)
  const merchantCommissionInvoiceNumber = buildData.genMerchantCommissionInvoiceNumber(purchase.id, merchant.id)
  const result = []
  let purchaseInvoiceId = ''
  let adminFeeInvoiceId = ''
  _.forEach(body.invoices, invoice => {
    const data = {
      financeId: invoice.invoiceID,
      purchaseId: purchase.id,
      amount: invoice.total,
      status: invoice.status,
      label: ''
    }
    switch (invoice.invoiceNumber) {
      case purchaseInvoiceNumber:
        data.label = PURCHASE_ACCOUNTING.LABEL.PURCHASE_RECEIVABLE_INVOICE_ID
        purchaseInvoiceId = invoice.invoiceID
        break
      case adminFeeInvoiceNumber:
        data.label = PURCHASE_ACCOUNTING.LABEL.ADMIN_FEE_RECEIVABLE_INVOICE_ID
        adminFeeInvoiceId = invoice.invoiceID
        break
      case merchantPayInvoiceNumber:
        data.label = PURCHASE_ACCOUNTING.LABEL.MERCHANT_PAYABLE_INVOICE_ID
        break
      case merchantCommissionInvoiceNumber:
        data.label = PURCHASE_ACCOUNTING.LABEL.MERCHANT_COMMISSION_RECEIVABLE_INVOICE_ID
        break
    }
    result.push(data)
  })
  return {invoiceIds: result, purchaseInvoiceId, adminFeeInvoiceId}
}
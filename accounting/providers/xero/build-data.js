const {XERO, TRANSACTION_FEE, MERCHANT_FINANCIAL, DEFAULT_MERCHANT_COMMISION} = require('lit-constants')
const _ = require('lodash')
const moment = require('moment')

const genAdminFeeInvoiceNumber = (purchaseId) => `receivable_admin_fee_purchase#${purchaseId}`
const genPurchaseInvoiceNumber = (purchaseId) => `receivable_purchase#${purchaseId}`
const genMerchantCommissionInvoiceNumber = (purchaseId, merchantId) => `receivable_commission_merchant#${merchantId}_purchase#${purchaseId}`
const genMerchantPayInvoiceNumber = (purchaseId, merchantId) => `payable_merchant#${merchantId}_purchase#${purchaseId}`
const getPaymentGatewayPayInvoiceNumber = (purchaseId, i) => `payable_PG_fee_installment#${i}_purchase#${purchaseId}`


const buildAdminFeeInvoice = (purchase, dueDate, user) => ({
  type: XERO.INVOICE.TYPE.ACCREC,
  contact: {
    contactID: user.financeId
  },
  dueDate,
  lineAmountTypes: XERO.LINE_ACCOUNT_TYPE.EXCLUSIVE,
  lineItems: [
    {
      description: `PR#${purchase.id} - Admin fees`,
      quantity: '1',
      unitAmount: `${purchase.adminFeeAmount || TRANSACTION_FEE}`,
      accountCode: XERO.REV_ADMIN_FEES
    }
  ],
  invoiceNumber: genAdminFeeInvoiceNumber(purchase.id),
  status: XERO.INVOICE.STATUS.AUTHORISED
})

const buildPurchaseRecInvoice = (purchase, dueDate, user, merchant) => ({
  type: XERO.INVOICE.TYPE.ACCREC,
  contact: {
    contactID: user.financeId
  },
  dueDate,
  lineAmountTypes: XERO.LINE_ACCOUNT_TYPE.EXCLUSIVE,
  lineItems: [
    {
      description: `PR#${purchase.id} - ${merchant.name}`,
      quantity: '1',
      unitAmount: `${purchase.amount}`,
      accountCode:  XERO.REV_USER_RECEIVABLE
    }
  ],
  invoiceNumber: genPurchaseInvoiceNumber(purchase.id),
  status: XERO.INVOICE.STATUS.AUTHORISED
})

const buildMerchantPayInvoice = (purchase, dueDate, user, merchant) => ({
  type: XERO.INVOICE.TYPE.ACCPAY,
  contact: {
    contactID: merchant.financeId
  },
  dueDate,
  lineAmountTypes: XERO.LINE_ACCOUNT_TYPE.EXCLUSIVE,
  lineItems: [
    {
      description: `PR#${purchase.id} - ID${user.id} - ${user.firstName} ${user.lastName}`,
      quantity: '1',
      unitAmount: `${purchase.amount}`,
      accountCode: XERO.COST_MERCHANT
    }
  ],
  invoiceNumber: genMerchantPayInvoiceNumber(purchase.id, merchant.id),
  status: XERO.INVOICE.STATUS.AUTHORISED
})

const _getMerchantCommissionAmount = (commission, amount) => {
  if (!commission) {
    return _.round(amount * DEFAULT_MERCHANT_COMMISION / 100, 2)
  }
  return commission.paymentType === MERCHANT_FINANCIAL.TYPE.AMOUNT
    ? commission.paymentAmount
    : _.round(amount * (commission.paymentAmount / 100))

}

const buildMerchantCommissionInvoice = (purchase, dueDate, user, merchant, commission) => {
  const amount = _getMerchantCommissionAmount(commission, purchase.amount)
  return {
    type: XERO.INVOICE.TYPE.ACCREC,
    contact: {
      contactID: user.financeId
    },
    dueDate,
    lineAmountTypes: XERO.LINE_ACCOUNT_TYPE.EXCLUSIVE,
    lineItems: [
      {
        description: `PR#${purchase.id} - ID${user.id} - ${user.firstName} ${user.lastName}`,
        quantity: '1',
        unitAmount: amount,
        accountCode: XERO.REV_MERCHANT_FEES,
        discountRate: '0'
      }
    ],
    invoiceNumber: genMerchantCommissionInvoiceNumber(purchase.id, merchant.id),
    status: XERO.INVOICE.STATUS.AUTHORISED
  }
}

const buildPaymentGatewayFeeInvoices = (desc, amount, financeId) => {
  const dueDate = moment().format(XERO.DATE_FORMAT)
  return {
    type: XERO.INVOICE.TYPE.ACCPAY,
    contact: {
      contactID: financeId
    },
    dueDate,
    lineAmountTypes: XERO.LINE_ACCOUNT_TYPE.EXCLUSIVE,
    lineItems: [
      {
        description: desc,
        quantity: '1',
        unitAmount: amount,
        accountCode: XERO.COST_PG
      }
    ],
    status: XERO.INVOICE.STATUS.AUTHORISED
  }
}


const buildPaymentGatewayPayment = (amount, invoiceId, userId) => {
  return {
    invoice: { invoiceID: invoiceId },
    account: { code: XERO.BANK1 },
    amount,
    details: `UserID#${userId} - Payment gateway fee`,
  }
}
const buildPurchaseData = (purchase) => {
  const user = _.get(purchase, 'User', {})
  const merchant = _.get(purchase, 'Merchant', {})
  const commission = _.get(merchant, 'MerchantFinancials.0', {})
  if (!user.financeId || !merchant.financeId) {
    return null
  }
  const dueDate = moment().format(XERO.DATE_FORMAT)
  const invoices = []
  invoices.push(buildAdminFeeInvoice(purchase, dueDate, user))
  invoices.push(buildPurchaseRecInvoice(purchase, dueDate, user, merchant))
  invoices.push(buildMerchantPayInvoice(purchase, dueDate, user, merchant))
  invoices.push(buildMerchantCommissionInvoice(purchase, dueDate, user, merchant, commission))
  return invoices
}

const buildInstallmentPayment = (installment, purchaseInvoiceId, purchase, merchant) => {
  return {
    invoice: { invoiceID: purchaseInvoiceId },
    account: { code: XERO.BANK1 },
    amount: installment.installmentNumber === 1 ? installment.amount - (purchase.adminFeeAmount || TRANSACTION_FEE) : installment.amount,
    reference: installment.id, // quick solution for attach installment ID with the invoice, so we don't have to check
    // which invoice belong to which installment
    details:  `PR#${purchase.id} -${merchant.name} - payment no.${installment.installmentNumber}`,
  }
}

const buildAdminFeePayment = (purchase, adminFeeInvoiceId) => {
  return {
    invoice: { invoiceID: adminFeeInvoiceId },
    account: { code: XERO.BANK1 },
    amount: `${purchase.adminFeeAmount || TRANSACTION_FEE}`,
    reference: `admin_fee_payment_for_purchase#${purchase.id}`,
    details: `UserID#${purchase.userId} - Admin fees`,
  }
}

module.exports = {
  genAdminFeeInvoiceNumber,
  genPurchaseInvoiceNumber,
  genMerchantCommissionInvoiceNumber,
  genMerchantPayInvoiceNumber,
  getPaymentGatewayPayInvoiceNumber,
  buildAdminFeeInvoice,
  buildPurchaseRecInvoice,
  buildMerchantPayInvoice,
  buildMerchantCommissionInvoice,
  buildPaymentGatewayFeeInvoices,
  buildPurchaseData,
  buildInstallmentPayment,
  buildAdminFeePayment,
  buildPaymentGatewayPayment,
}
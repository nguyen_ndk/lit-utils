const aws = require('aws-sdk');
const _ = require('lodash');
const { STORAGE, AWS } = require('lit-constants');
const s3Zip = require('s3-zip');
const Archiver = require('archiver');
const mime = require('mime-types');
const path = require('path');

class S3Provider {
  constructor() {
    this.s3 = new aws.S3({
      accessKeyId: AWS.ACCESS_KEY,
      secretAccessKey: AWS.ACCESS_SECRET,
    });
    this.defaultBucket = AWS.S3_DEFAULT_BUCKET;
  }
  put(filePath, content, options) {
    const mimeType = mime.contentType(path.extname(filePath));
    const params = {
      Bucket: _.get(options, 'destination', this.defaultBucket),
      ACL: 'public-read',
      Key: filePath,
      Body: content,
      ContentType: mimeType,
    };

    // Uploading files to the bucket
    return new Promise((resolve, reject) => {
      this.s3.upload(params, function (err, data) {
        if (err) return reject(err);
        resolve(data);
      });
    });
  }

  uploadKycResponse(filePath, content) {
    const params = {
      Bucket: STORAGE.KYC,
      Key: filePath,
      Body: JSON.stringify(content),
      ContentType: 'application/json; charset=utf-8',
    };
    return new Promise((resolve, reject) => {
      this.s3.upload(params, function (err, data) {
        if (err) return reject(err);
        resolve(data);
      });
    });
  }

  zip(folder, res, bucket) {
    const self = this;
    folder += '/';
    const params = {
      Bucket: bucket,
      Prefix: folder,
    };

    self.getListingS3(params).then((keys) => {
      const filesArray = [];
      for (const key of keys) {
        filesArray.push(key.substr(folder.length));
      }

      self.doZip(filesArray, res, bucket, folder);
    });
  }

  doZip(files, res, bucket, folder) {
    if (_.isEmpty(files)) {
      const zip = new Archiver('zip');
      zip.pipe(res);
      zip.finalize();
      return;
    }
    s3Zip
      .archive(
        {
          region: AWS.REGION,
          bucket: bucket,
          preserveFolderStructure: true,
          s3: this.s3,
          debug: true,
        },
        folder,
        files,
      )
      .pipe(res);
  }

  getListingS3(params) {
    var that = this;
    return new Promise((resolve, reject) => {
      try {
        const allKeys = [];
        listAllKeys();
        function listAllKeys() {
          that.s3.listObjectsV2(params, function (err, data) {
            if (err) {
              reject(err);
            } else {
              var contents = data.Contents;
              contents.forEach(function (content) {
                allKeys.push(content['Key']);
              });

              if (data.IsTruncated) {
                params.ContinuationToken = data.NextContinuationToken;
                console.log('get further list...');
                listAllKeys();
              } else {
                resolve(allKeys);
              }
            }
          });
        }
      } catch (e) {
        reject(e);
      }
    });
  }
}

module.exports = new S3Provider();

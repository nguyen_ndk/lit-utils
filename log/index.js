const winston = require('./providers/winston')
const LOG_PROVIDER = {
  WINSTON: 'winston',
}
class Log {
  static provider(provider) {
    switch (provider) {
      case LOG_PROVIDER.STRIPE:
        return winston
      default:
        return winston
    }
  }

  static info(...args) {
    return Log.provider().info(...args)
  }

  static silly(...args) {
    return Log.provider().silly(...args)
  }

  static debug(...args) {
    return Log.provider().debug(...args)
  }

  static verbose(...args) {
    return Log.provider().verbose(...args)
  }

  static warn(...args) {
    return Log.provider().warn(...args)
  }

  static error(...args) {
    return Log.provider().error(...args)
  }

}

module.exports = {
  Log,
  LOG_PROVIDER
}
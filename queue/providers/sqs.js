const aws = require('aws-sdk')
const { Consumer } = require('sqs-consumer')
const _ = require('lodash')
const {AWS} = require('lit-constants')

class SQSProvider {
  constructor() {
    const config = {
      accessKeyId: AWS.ACCESS_KEY,
      secretAccessKey: AWS.ACCESS_SECRET,
      region: AWS.REGION,
    }
    this.sqs = new aws.SQS(config)
    this.defaultQueue = AWS.SQS_DEFAULT_QUEUE
  }

  send(body, options) {
    const params = {
      DelaySeconds: _.get(options, 'delaySeconds', 10),
      MessageBody: body,
      QueueUrl: _.get(options, 'queue', this.defaultQueue)
    }
    return new Promise(((resolve, reject) => {
      this.sqs.sendMessage(params, (e, res) => {
        if (e) return reject(e)
        resolve(res)
      })
    }))
  }

  async createConsumer(queue, handler, errorHandler, processingErrorHandler) {
    const consumer = Consumer.create({
      queueUrl: queue,
      handleMessage: handler,
      sqs: this.sqs
    })

    consumer.on('error', errorHandler)

    consumer.on('processing_error', processingErrorHandler)

    consumer.start()
  }

  async sendBatch(messages, options) {
    const params = {
      DelaySeconds: _.get(options, 'delaySeconds', 10),
      QueueUrl: _.get(options, 'queue', this.defaultQueue),
      Entries: messages
    }
    await this.sqs.sendMessageBatch(params).promise()
  }
}

module.exports = new SQSProvider()

const {AUTH} = require('lit-constants')
const {EncryptFacade} = require('../encrypt')
const { v4: uuidv4 } = require('uuid')

const generateSecretKey = merchantId => {
  return EncryptFacade.encrypt(JSON.stringify({id: merchantId, uniq: uuidv4()}), AUTH.MERCHANT.SECRET.API)
}

module.exports = {
  generateSecretKey
}
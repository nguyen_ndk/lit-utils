const axios = require('axios')
const FormData = require('form-data')
const _ = require('lodash')
const KALAPA_BASE_URL = 'https://api.kalapa.vn'
const {KALAPA} = require('lit-constants')

const ID_CARD_PLUS_MAPPING = [
  {field: 'careerName', path: 'careerName', default: ''},
  {field: 'careerDob', path: 'careerDob', default: ''},
  {field: 'careerAddr', path: 'careerAddr', default: ''},
  {field: 'careerGender', path: 'careerGender', default: ''},
  {field: 'verified', path: 'verified', default: false},

  {field: 'idCardInfoId', path: 'idCardInfo.id', default: ''},
  {field: 'idCardInfoName', path: 'idCardInfo.name', default: ''},
  {field: 'idCardInfoAddress', path: 'idCardInfo.address', default: ''},
  {field: 'idCardInfoDob', path: 'idCardInfo.dob', default: ''},
  {field: 'idCardInfoHome', path: 'idCardInfo.home', default: ''},
  {field: 'idCardInfoExpiredDate', path: 'idCardInfo.expiredDate', default: ''},
  {field: 'idCardInfoDocumentType', path: 'idCardInfo.documentType', default: ''},
  {field: 'idCardInfoGender', path: 'idCardInfo.gender', default: ''},

  {field: 'selfieCheckMatched', path: 'selfieCheck.matched', default: false},
  {field: 'selfieCheckMatchingScore', path: 'selfieCheck.matchingScore', default: 0},
  {field: 'selfieCheckErrorMsg', path: 'selfieCheck.errorMsg', default: ''},
  {field: 'selfieCheckErrorCode', path: 'selfieCheck.errorCode', default: 0},

  {field: 'fraudCheckFacesMatched', path: 'fraudCheck.facesMatched', default: 0},
  {field: 'fraudCheckFloatingMark', path: 'fraudCheck.floatingMark', default: false},
  {field: 'fraudCheckScreenPhoto', path: 'fraudCheck.screenPhoto', default: false},
  {field: 'fraudCheckBlacklist', path: 'fraudCheck.blacklist', default: false},
  {field: 'fraudCheckAbnormalText', path: 'fraudCheck.abnormalText', default: false},
  {field: 'fraudCheckAbnormalFace', path: 'fraudCheck.abnormalFace', default: false},
  {field: 'fraudCheckCornerCut', path: 'fraudCheck.cornerCut', default: false},
]

const ANTI_FRAUD_MAPPING = [
  {field: 'score', path: 'score', default: '0'},
  {field: 'numOrg', path: 'numOrg', default: 0},
  {field: 'percentile', path: 'percentile', default: 0},
]

const CREDIT_MAPPING = [
  {field: 'numberOfLoans', path: 'numberOfLoans', default: 0},
  {field: 'hasBadDebt', path: 'hasBadDebt', default: true},
  {field: 'hasLatePayment', path: 'hasLatePayment', default: true},
]

const SCORE_PLUS_MAPPING = [
  {field: 'medicalNumber', path: 'medicalNumber', default: ''},
  {field: 'isCurrentEmployed', path: 'isCurrentEmployed', default: false},
  {field: 'salaryLevel', path: 'salaryLevel', default: 0},
]

const PROVIDER = 'kalapa'

const request = axios.create({
  baseURL: KALAPA_BASE_URL,
  headers: {
    Authorization: KALAPA.API_KEY
  }
})

const getUserInfoByIDImage = async (idPhoto, selfiePhoto) => {
  const formData = new FormData()
  formData.append('image ', idPhoto.data, {
    contentType: idPhoto.mimetype,
    filename: idPhoto.name,
  })
  formData.append('image_selfie  ', selfiePhoto.data,
    {
      contentType: selfiePhoto.mimetype,
      filename: selfiePhoto.name,
    })
  const api = '/user-profile/id_card/plus/?verify=true'
  const {data} = await request.post(api, formData, {headers: formData.getHeaders()})
  return {data, api, provider: PROVIDER}
}

const antifraud = async (id) => {
  const api = '/user-profile/antifraud/get/'
  const {data} = await request.get(`${api}?id=${id}&version=GENERAL_PDL`)
  return {data, api, provider: PROVIDER}
}

const creditInfo = async (id) => {
  const api = '/user-profile/credit/v2/get'
  const {data} = await request.get(`${api}?id=${id}`)
  return {data, api, provider: PROVIDER}
}

const userScore = async (id) => {
  const api = '/user-profile/score-plus/get/'
  const {data} = await request.get(`${api}?id=${id}`)
  return {data, api, provider: PROVIDER}
}

const mapping = (res, mapping) => {
  const result = {}
  _.forEach(mapping, (config) => {
    result[config.field] = _.get(res, config.path, config.default)
  })
  result.provider = PROVIDER
  return result
}

const mappingIdInfo = (res) => mapping(res, ID_CARD_PLUS_MAPPING)
const mappingAntifraud = (res) => mapping(res, ANTI_FRAUD_MAPPING)
const mappingScore = (res) => mapping(res, SCORE_PLUS_MAPPING)
const mappingCredit = (res) => mapping(res, CREDIT_MAPPING)

module.exports = {
  creditInfo,
  userScore,
  getUserInfoByIDImage,
  mapping,
  mappingIdInfo,
  mappingAntifraud,
  mappingScore,
  mappingCredit,
  antifraud,
}
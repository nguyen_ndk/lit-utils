/* eslint-disable camelcase */
const OneSignal = require('onesignal-node')
const {ONESIGNAL} = require('lit-constants')

const userClient = new OneSignal.Client(
  ONESIGNAL.USER_APP_ID,
  ONESIGNAL.USER_API_KEY,
)

const mobilePosClient = new OneSignal.Client(
  ONESIGNAL.POS_APP_ID,
  ONESIGNAL.POS_API_KEY,
)

const sendPos = async (headings, contents, data, pushNotificationIds) => {
  const notification = {
    contents,
    include_player_ids: pushNotificationIds,
    data
  }
  return mobilePosClient.createNotification(notification)
}

const sendUser = async (headings, contents, data, pushNotificationIds) => {
  const notification = {
    contents,
    include_player_ids: pushNotificationIds,
    data
  }
  return userClient.createNotification(notification)
}

module.exports = {
  sendPos,
  sendUser
}
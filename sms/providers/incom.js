const axios = require('axios')
const {INCOM} = require('lit-constants')


class Incom {

  sendSMS(phoneNumber, template){
    const data = {
      Username: INCOM.USERNAME,
      Password: INCOM.PASSWORD,
      PhoneNumber: phoneNumber,
      PrefixId: INCOM.PREFIXED,
      CommandCode: INCOM.COMMANDCODE,
      RequestId: '0',
      MsgContent: template,
      MsgContentTypeId: 0,
      FeeTypeId: 0
    }
    return axios.post(INCOM.INCOM_SMS_URL, data)
      .then((apiResponse) => {
        return {
          result: apiResponse.data.StatusCode,
          message: JSON.stringify(apiResponse.data),
          carrier: ''
        }
      })
      .catch((error) => {
        return {
          result: 0,
          message: JSON.stringify(error),
          carrier: ''
        }
      })
  }

}

module.exports = new Incom()

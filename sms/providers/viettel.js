// ! Deprecated not use anymore
// const soapRequest = require('easy-soap-request');
// const fs = require('fs');
// const url = 'http://ams.tinnhanthuonghieu.vn:8009/bulkapi?wsdl';
// const _ = require('lodash');
// const parser = require('xml2json');
// const { VIETTEL } = require('lit-constants');

// const sampleHeaders = {
//   'Content-Type': 'text/xml',
// };
// const xmlFile = fs.readFileSync('./src/utils/sms/providers/viettel-body.xml', 'utf-8');

// class ViettelProvider {
//   constructor() {
//     this.SMS_USER = VIETTEL.USER;
//     this.SMS_PASSWORD = VIETTEL.PASSWORD;
//     this.SMS_CPCODE = VIETTEL.CPCODE;
//     this.SMS_BRANDNAME = VIETTEL.BRAND_NAME;
//   }

//   async initSMS(phoneNumber, message) {
//     var xmlBody = xmlFile
//       .replace(/{USER}/g, this.SMS_USER)
//       .replace(/{PASSWORD}/g, this.SMS_PASSWORD)
//       .replace(/{CP_CODE}/g, this.SMS_CPCODE)
//       .replace(/{BRAND_NAME}/g, this.SMS_BRANDNAME)
//       .replace(/{PHONE_NUMBER}/g, phoneNumber)
//       .replace(/{MESSAGE}/g, message)

//     const {response} = await soapRequest({url: url, headers: sampleHeaders, xml: xmlBody, timeout: 1000})
//     const {body} = response
//     var json = parser.toJson(body, {object:true})

//     return {
//       result: json['S:Envelope']['S:Body']['ns2:wsCpMtResponse']['return']['result'],
//       message: json['S:Envelope']['S:Body']['ns2:wsCpMtResponse']['return']['message'],
//       carrier: await this.getCarrier(json['S:Envelope']['S:Body']['ns2:wsCpMtResponse']['return']['message'])
//     }

//     return;
//   }

//   getCarrier(response) {
//     if (response.includes('IP_INVALID')) {
//       return 'IP_INVALID';
//     }

//     var regExp = new RegExp('TELCO=(.*)');
//     var result = response.match(regExp);

//     return _.get(result, [1], 0);
//   }
// }

// module.exports = new ViettelProvider();

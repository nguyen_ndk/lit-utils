const url = 'https://api-02.worldsms.vn/webapi/sendSMS'
const _ = require('lodash')
const axios = require('axios')
const {SOUTH_TELECOM} = require('lit-constants')


class SouthtelecomProvider {
  constructor() {
    this.SMS_ST_AUTHORIZATION = SOUTH_TELECOM.AUTHORIZATION
    this.SMS_ST_BRANDNAME = SOUTH_TELECOM.BRAND_NAME
  }

  async initSMS(phoneNumber, message) {
    const data = {
      from: this.SMS_ST_BRANDNAME,
      to: phoneNumber,
      text: message+' Sent from VienThongMN Demo Env LITPAY.' //Template test
    }

    var response = axios.post(url, data, {
      headers: {
        Authorization: 'Basic ' + this.SMS_ST_AUTHORIZATION
      }
    }).then((apiResponse) => {
      return {
        result: apiResponse.data.status,
        message: JSON.stringify(apiResponse.data),
        carrier: apiResponse.data.carrier
      }
    }, (error) => {
      return {
        result: 0,
        message: JSON.stringify(error),
        carrier: ''
      }
    })

    return response
  }

  getCarrier(response) {
    if (response.includes('IP_INVALID')) {
      return 'IP_INVALID'
    }

    var regExp = new RegExp('TELCO=(.*)')
    var result = response.match(regExp)

    return _.get(result, [1], 0)
  }


}

module.exports = new SouthtelecomProvider()
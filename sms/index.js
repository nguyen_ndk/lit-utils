const incom = require('./providers/incom')

const SMS_PROVIDER = {
  INCOM: 'incom',
}

class SmsFacade {
  static provider(provider) {
    switch (provider) {
      case SMS_PROVIDER.INCOM:
        return incom
      default:
        return incom
    }
  }

  send(phoneNumber, template) {
    return SmsFacade.provider().sendSMS(phoneNumber, template)
  }
}


module.exports = new SmsFacade(SMS_PROVIDER.INCOM)
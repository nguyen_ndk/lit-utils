const jwt = require('jsonwebtoken')

const encrypt = (data, secret) => {
  return jwt.sign(data, secret)
}

const decrypt = (encryptedSrc, secret) => {
  return jwt.verify(encryptedSrc, secret)
}

module.exports = {
  encrypt,
  decrypt
}
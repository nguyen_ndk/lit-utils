const jwt = require('./providers/jwt')

const ENCRYPT_PROVIDER = {
  JWT: 'jwt'
}

class EncryptFacade {
  static provider(provider) {
    switch (provider) {
      case ENCRYPT_PROVIDER.JWT:
        return jwt
      default:
        return jwt
    }
  }

  static encrypt(data, secret) {
    return EncryptFacade.provider().encrypt(data, secret)
  }
  static decrypt(encryptedStr, secret) {
    return EncryptFacade.provider().decrypt(encryptedStr, secret)
  }
}

module.exports = {
  ENCRYPT_PROVIDER,
  EncryptFacade
}
